'use strict';

module.exports = {
  url: 'https://devheroesforus.netlify.com/',
  title: 'Developer Heroes',
  subtitle: 'Official Website for Developer Heroes TI UMY',
  copyright: '© DevHeroes, 2019.',
  disqusShortname: 'https-devheroesforus-netlify-com',
  postsPerPage: 4,
  googleAnalyticsId: '',
  menu: [
    {
      label: 'Articles',
      path: '/'
    },
    {
      label: 'About Us',
      path: '/pages/about'
    },
    {
      label: 'Activities',
      path: '/pages/activities'
    }
  ],
  author: {
    name: 'Developer Heroes',
    photo: '/logo.png',
    bio: 'Ini Adalah Website Resmi Developer Heroes TI UMY',
    contacts: {
      email: 'devheroes00@gmail.com',
      telegram: 'devheroesumy',
      twitter: 'wahudamon',
      github: 'Wahudamon',
      instagram: 'https://www.instagram.com/devheroes19/',
      facebook: 'https://www.facebook.com/wahudakun'
    }
  }
};
