---
title: Pengulangan pada Pemrograman
date: "2019-11-18"
template: "post"
draft: false
slug: "/posts/loop-control-statement/"
category: "Learn"
tags:
  - "DartLang"
  - "Pemrograman"
  - "Pengulangan"
description: "Mengulang sebuah proses sampai hasilnya sesuai dengan yang ditentukan"
---

What's up?<br>
Halo temen-temen, MinDep balik lagi!

Kemarin kita udah belajar tentang control flow statement, asik kan ya? kita bisa bermain dengan kondisi dan bisa menentukan arah bagaimana masukkan atau input kita diproses sampai mendapatkan hasil yang diinginkan.

Dan sekarang kita bakal belajar tentang apa itu yang namanya Loop Control Statement. Yups, kata kuncinya adalah **Looping** atau **Perulangan**.

Langsung aja ya!

Jadi Loop Control itu adalah metode yang berfungsi untuk mengeksekusi kode berkali-kali atau terus diulang sampai batas waktu atau kondisi tertentu, yang biasanya sudah ditentukan oleh pemrogram atau programmer.

Jadi misalnya temen-temen ingin menampilkan nama sebanyak 100 kali pada program. Jika dengan cara manual kalian harus menuliskan sintaks print sebanyak 100 baris, nah dengan konsep perulangan ini kalian hanya perlu menuliskan sekitar 3 baris saja untuk memberikan perintah tersebut pada program.

Beberapa teknik Loop Control yang ada di Bahasa Pemrograman Dart adalah :
- **FOR**

Looping dengan dikontrol oleh tiga bagian yang ada dalam tanda kurung dan masing-masing bagian ini dipisahkan oleh titik koma. Pada bagian pertama, sebuah variabel akan dideklarasikan sebagai titik awal dari perulangan, biasanya variabel ini mempunyai tipe data integer atau float.

Sementara pada bagian kedua, disini lah perulangan akan diperiksa apakah masih memenuhi syarat atau tidak, jika masih memenuhi syarat maka statement dibawahnya akan dieksekusi. Sedangkan bagian ketiga adalah bagian dimana jika bagian kedua masih memenuhi syarat maka nilai variabel akan ditambahkan sesuai dengan syarat yang sudah dituliskan.

Bagian ketiga ini secara otomatis akan tidak dibaca oleh program jika bagian kedua sudah tidak lagi memenuhi syarat, dan perulangan pun menjadi terhenti.

Contohnya seperti ini :

```dart
main() {
    // int i = 0 adalah bagian pertama
    // i < 10 adalah bagian kedua
    // i++ adalah bagian ketiga
    for(int i = 0; i < 10; i++) {
        print('Pernyataan ini diulang menggunakan Teknik Looping FOR.');
    }
}
```

Penjelasannya seperti ini, di bagian pertama MinDep mendeklarasikan sebuah variabel sebagai titik awal dimulainya perulangan. Kemudian pada bagian kedua adalah syarat perulangannya. Dan pada bagian terakhir atau ketiga adalah untuk menambahkan nilai pada variabel i ketika syarat pada bagian kedua belum terpenuhi.

Sehingga perintah untuk menampilkan tulisan **Pernyataan ini diulang menggunakan Teknik Looping FOR.** akan terus dijalankan sampai syarat pada bagian kedua sudah tidak terpenuhi lagi atau variabel i memiliki nilai 10.

- **WHILE**

Teknik ini hampir sama dengan IF, yaitu dengan adanya sebuah kondisi. Namun di teknik while ini, selama kondisi tersebut bernilai true atau benar maka pengulangan akan terus dilanjutkan. Sampai akhirnya kondisi tersebut bernilai false atau salah.

MinDep bakal memberikan contohnya tentang simulasi login (Seperti bahasan di grup DEV HEROES beberapa waktu yang lalu) yaitu dengan masukkan username dan password, program akan mendeteksi kalau misalnya username dan password tidak sesuai maka akan menampilkan kalimat "Gagal, username dan password salah"

Nah jika username dan password sesuai, maka akan menampilkan kalimat "Login Berhasil". Langsung kita coba aja ya gengs!

```dart
import 'dart:io';

main() {
    print("DevHeroes App 1.0");
    print("Username : ");
    var uname = stdin.readLineSync();

    print("Password : ");
    var pass = stdin.readLineSync();

    while (uname != "bagas" && pass != "1234") {
        print("\nGagal, username atau password salah\n");

        print("Username : ");
        uname = stdin.readLineSync();

        print("Password : ");
        pass = stdin.readLineSync();
    }

    print("\nLogin Berhasil!");
}
```

Penjelasannya gini, pertama-tama pengguna memasukkan nilai dengan bantuan library [dart:io](https://api.dartlang.org/stable/2.6.1/dart-io/dart-io-library.html) sehingga program bisa membaca dan menampung nilai yang kita masukkan pada sebuah variabel.

Kemudian menampung kedalam variabel uname untuk username dan pass untuk password. Lalu setelah itu menuliskan while dengan kondisi bahwa nilai yang ada di variabel uname dan pass tidak sama dengan username dan password sebenarnya yang sudah MinDep buat.

Didalam while tersebut MinDep buat perintah untuk menampilkan kalimat "Gagal, username dan password salah" dan menampilkan kembali tempat untuk si pengguna untuk memasukkan nilai username dan password kembali.

Dan hal itu akan terus diulang selama sesuai dengan pernyataan kondisi yang ada di dalam while. Kondisi username dan password benar adalah kondisi di luar yang ada di dalam while itu tadi.

Nah jika username dan password benar, maka kemudian perintah yang ada di dalam while itu tadi tidak akan dieksekusi dan lanjut ke baris selanjutnya dimana MinDep buat untuk menampilkan kalimat "Login Berhasil".

Dan masih banyak teknik looping yang lainnya. MinDep selalu mengingatkan kalo mencari itu indah, Hehehe.

Sampe sini dulu buat bahasan tentang Loop Control Statement. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*

Sumber lain :<br>
[student.blog.dinus.ac.id](http://student.blog.dinus.ac.id/rezanda/model-pemrograman/#:~:targetText=Loop%20atau%20perulangan%20adalah%20suatu,atau%20perulangan%20sangat%20berguna%20sekali.)