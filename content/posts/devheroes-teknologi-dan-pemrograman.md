---
title: Tentang DevHeroes, Teknologi, dan Pemrograman
date: "2019-11-1"
template: "post"
draft: false
slug: "/posts/devheroes-teknologi-dan-pemrograman/"
category: "Learn"
tags:
  - "Komunitas"
  - "Pemrograman"
  - "Teknologi"
description: "Tak kenal maka tak sayang, yuk kita kenalan dulu!"
---

![DevHeroes, Teknologi, dan Pemrograman](/media/learn/artikel1thumbnail.png)

Selamat pagi, siang, sore, dan malem buat temen-temen yang baca tulisan ini!

Udah beberapa minggu ini kita belajar bareng, banyak materi yang udah temen-temen bawa pulang kekos dan kerumah masing-masing. Yaaak mimin cuman mau mengucapkan selamat datang dan kuy belajar bareng di Developer Heroes!

Ini adalah website resmi dari Developer Heroes, dan perkenalkan aku adalah **Mimin Devy**.. Ehm panggil aja *MinDep*. Mimin yang paling keren di DevHeroes dan yang bakalan terus membuatkan tulisan-tulisan yang mudah-mudahan bermanfaat buat temen-temen semuanya di website ini.

DevHeroes, DevHeroes, DEVHEROES TERUS! APA SIH DEVHEROES NI?

Yak, di artikel pertama ini MinDep bakal menjelaskan tentang siapa sih Developer Heroes atau yang sering dipanggil DevHeroes ini. Dan sedikit bercerita tentang teknologi dan tentunya masalah pemrograman yang bakalan jadi materi utama yang dibahas setiap pertemuan kita.

### Tentang Developer Heroes
Developer Heroes atau DevHeroes adalah sebuah komunitas yang dibuat pada tanggal 30 September 2019. Yang mana latar belakang dari dibuatnya komunitas ini adalah tentang keadaan sekarang yang ada di lingkungan Prodi Teknik Informatika UMY.

Kami melihat kurangnya minat mahasiswa TI UMY untuk mempelajari dan mendalami ilmu yang berkaitan tentang bidang teknologi terutama tentang pemrograman. Lalu kami berinisiatif untuk membuat sebuah komunitas sebagai wadah untuk belajar bersama diluar perkuliahan tentang kedua hal tersebut. Ya, tentang teknologi dan pemrograman.

Karena menurut kami, minat akan muncul ketika belajar bersama dengan yang lain. Bersama-sama membuat sesuatu yang berguna dengan teknologi dan pemrograman.

### Teknologi Di Sekitar Kita
Secara tidak sadar, teknologi selalu ada disamping kita mulai dari membuka mata di pagi hari sampai akhirnya kita berada diatas kasur kembali untuk tidur di malam hari. Di pagi hari setelah bangun tidur, mimin percaya pasti benda pertama yang dicari oleh kalian adalah Smartphone kalian sendiri. *Ngaku deeeh hahaha*.

Kemudian berangkat ke kampus atau kantor menggunakan sepeda motor yang merupakan produk dari teknologi juga. Lalu sesampainya di kampus atau kantor, temen-temen langsung membuka laptop untuk cek apakah hari ini ada tugas atau enggak. Buka Smartphone juga sebentar lalu buka Instagram atau Twitter buat stalking gebetan selagi sempat, hahaha.

Itu merupakan contoh penerapan teknologi yang bisa ada di bidang apa saja. Nah berarti kalo gitu semakin menarik dong untuk dipelajari min? Ya jelaslah, sederhananya kalian bisa membuat sesuatu yang bisa memecahkan masalah kalian sehari-hari dengan bantuan teknologi. Keren!

### Kenalan Sama Pemrograman
Nah tadi udah bahas tentang teknologi, gimana keren kan?

Sekarang kita bahas salah satu ilmu yang ada di bidang teknologi, yaitu pemrograman atau programming atau istilah kerennya coding.

Salah satu produk digital yang sering kita gunakan adalah aplikasi. Nah aplikasi ini dijalankan pada sebuah mesin yang bermacam-macam jenisnya, contohnya laptop dan smartphone.

Secara singkat, pemrograman adalah sebuah kegiatan dimana kita sebagai pemrogram (Programmer) memberikan instruksi kepada mesin dalam bentuk teks yang biasa disebut script atau code.

![Analogi Pemrograman](/media/learn/codinganalogy.png)

Gambar diatas ini adalah analogi pemrograman secara sederhana yang sesuai dengan penjelasan pada kalimat sebelumnya.

Naaah Itu tadi sedikit bahasan tentang DevHeroes, Teknologi, dan Pemrograman. Makasih banyak udah ngeluangin waktu buat baca artikel ini yaaaa.

Kalo pengen tau lebih jauh lagi tentang ketiga hal ini bisa ikut belajar bareng DevHeroes setiap hari jumat malam. Info lebih lanjutnya ada di Instagram dan Telegram DevHeroes yang bisa kalian akses dengan klik ikon nya yang ada di halaman beranda Website DevHeroes.

Dan sebelum belajar bareng DevHeroes, temen-temen wajib install terlebih dahulu Dart SDK yang bisa diklik tutorialnya pada gambar dibawah ini.

[![Tutorial Install Dart SDK](/media/animation/taphere.gif)](https://www.instagram.com/tv/B4jaWjsHf0Z/?igshid=4obb7a7j43vu "Tutorial Install Dart SDK")