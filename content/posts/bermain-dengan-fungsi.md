---
title: Bermain dengan Fungsi
date: "2019-11-26"
template: "post"
draft: false
slug: "/posts/bermain-dengan-fungsi/"
category: "Learn"
tags:
  - "DartLang"
  - "Pemrograman"
  - "Fungsi"
description: "Apa itu Fungsi?"
---

*Halo para heroes andalan MinDep, MinDep is back again!*

Gimana kabarnya sobat heroes kesayangan MinDep? <br>
Semoga kalian semua baik-baik aja yaa!

Di pertemuan sebelumnya kita bersama mas Miftah membahas tentang fungsi pada pemrograman. Nah ini juga merupakan salah satu dasar pemrograman yang penting buat temen-temen pahami.

Yuk kita sedikit review tentang pertemuan di minggu kemarin.

## Apa sih fungsi itu?

Jadi nih, sebenarnya program yang kita buat dan pakai itu terdiri dari beberapa subprogram yang berupa fungsi. Dimana setiap subprogram atau fungsi ini akan memiliki kegunaan tertentu yang berbeda-beda.

Pake analogi lagi deh yaa, biar sobat heroes ga pusing hehehe.

Jadi analoginya seperti kehidupan sehari-hari yang dijalanin sama sobat heroes. Nah kita anggap kehidupan sehari-hari ini adalah sebuah program/aplikasi. Nah kegiatan yang dilakukan pada kehidupan sehari-hari inilah yang dimaksud dengan fungsi.

Seperti makan dan minum, itu adalah dua subprogram atau fungsi yang berbeda. Dimana masing-masing memiliki tugas dan tujuan akhir. Yaitu subprogram makan untuk melakukan kegiatan makan, dan minum untuk kegiatan minum.

Kira-kira analoginya seperti itu. Sebenarnya ada dua jenis subprogram, yaitu fungsi dan prosedur. Seperti biasa MinDep selalu gak pernah bosen ngingetin kalo mencari itu indah, hehehe.

Kali ini, MinDep taruh beberapa link sumber di bawah ya. Biar sobat heroes gampang eksplornya.

## Keuntungan menggunakan fungsi

Nah berdasarkan penjelasan MinDep diatas, sobat heroes tentunya bisa nebak dong salau satu keuntungan menggunakan fungsi itu apa, hehehe.

Nah langsung MinDep jelasin aja deh tentang beberapa kelebihan ketika menggunakan fungsi, yaitu :
1. **Modularisasi**

Jadi Modular maksudnya di sini adalah konsep untuk memecah-mecah program yang rumit menjadi subprogram yang lebih kecil dan sederhana. Sehingga bisa memudahkan si pemrogram dalam membaca struktur program.

2. **Dapat digunakan ulang (reusable)**

Jadi untuk hal-hal yang sering dilakukan berulang-ulang, cukup dituliskan sekali saja dalam fungsi dan dapat dipanggil atau digunakan sewaktu-waktu bila diperlukan. Tidak perlu membuat lagi perintah yang sama, cukup gunakan fungsi yang sudah dibuat sebelumnya.

Ini sangat membantu dan mempermudah pekerjaan sang pemrogram, karena tidak perlu kerja dua kali. Cukup buat sekali dan bisa dipakai sewaktu-waktu jika diperlukan.

3. **Menjadikan kode yang ditulis lebih mudah untuk dibaca**

Dengan mengelompokkan perintah-perintah sebagai fungsi dengan nama fungsi yang sesuai, maka membaca kode pun akan menjadi semakin mudah.

## Struktur sebuah fungsi

Struktur dari sebuah fungsi itu kurang lebih seperti ini.

```dart
int penjumlahan(int angkaPertama, int angkaKedua) {
    int hasil = angkaPertama + angkaKedua;
    return hasil;
}

// int adalah Return Type
// penjumlahan adalah Function name
// int angkaPertama adalah Required Parameter
// int angkaKedua adalah Required Parameter
// return hasil adalah Return Value
```

## Contohnyaa..
Misalnya kita ingin membuat sebuah program yang menghitung luas dari beberapa bangun datar. Kita akan mengimplementasikan penggunaan fungsi. Langsung aja contohnya dibawah ini.

```dart
void main() {
  print("Luas persegi : ${luasPersegi(4)}");
  print("Luas persegi panjang : ${luasPersegiPanjang(6,4)}");
  print("Luas jajar genjang : ${luasJajarGenjang(7,3)}");
  print("Luas segitiga : ${luasSegitiga(4,5)}");
}

int luasPersegi(int sisi){
    return sisi * sisi * sisi;
}

int luasPersegiPanjang(int panjang, int lebar){
    return panjang * lebar;
}

int luasJajarGenjang(int alas, int tinggi){
    return alas * tinggi;
}

double luasSegitiga(double alas, double tinggi){
    return 0.5 * alas * tinggi;
}
```

Pada contoh di atas juga udah dituliskan bagaimana cara memanggil fungsi. Sangat mudah sekali, hanya dengan menuliskan nama fungsinya kemudian diikuti dengan tanda kurung yang di dalamnya diberikan nilai parameter jika dibutuhkan.

Sampe sini dulu buat bahasan tentang Loop Control Statement. MinDep gak pernah bosen mengingatkan kalo mencari itu indah, Hehehe. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*<br>
*Stay Healthy, Stay Coding. Sobat Heroes!*

Link Sumber :<br>
[blog.ilkom.unsri.ac.id](http://blog.ilkom.unsri.ac.id/blog/2015/11/17/perbedaan-prosedur-dan-fungsi-dalam-algoritma-pemrograman/)<br>
[student.blog.dinus.ac.id](http://student.blog.dinus.ac.id/anshari/2017/07/06/fungsi-dalam-algoritma-pemrograman/)<br>
[kodedasar.com](https://kodedasar.com/prosedur-dan-fungsi/)