---
title: Ngoding Dengan Berpatokan Pada Objek!
date: "2019-12-04"
template: "post"
draft: false
slug: "/posts/oop-buat-ngoding/"
category: "Learn"
tags:
  - "DartLang"
  - "Pemrograman"
  - "Objek"
description: "Mengenal salah satu paradigma pemrograman yaitu Pemrograman Berorientasi Objek"
---

Yo gengs, ketemu lagi kita!

Setelah kemarin MinDep berbagi tentang Exception Handling, kali ini mimin punya sesuatu lagi nih buat dibagiin ke kalian para sobat heroes kesayangan MinDep!

Kali ini MinDep bakal sharing tentang salah satu paradigma pemrograman yang sering digunakan oleh para pemrogram atau programmer dalam menulis baris kode di kode editor mereka.

### What is OOP?

OOP atau Object-Oriented-Programming yang Bahasa Indonesianya adalah Pemrograman Berorientasi Objek adalah sebuah paradigma pemrograman yang menjadikan objek sebagai acuan atau patokan bagi para pemrogram (Programmer) ketika ngoding atau sedang membuat aplikasi.

Menurut sumber yang MinDep dapatkan, **Objek** memiliki arti dalam kelas nomina atau kata benda sehingga objek dapat menyatakan nama dari seseorang, tempat, atau semua benda dan segala yang dibendakan.

Detail tentang objek dalam OOP dibagi menjadi 2 bagian, yaitu properties atau bagian-bagian yang ada pada objek dan behavior atau tingkah laku objek. Untuk membuat properties di dalam objek, menggunakan instance variable. Lalu untuk membuat behavior atau tingkah laku objek bisa dibuat menggunakan fungsi atau method.

### Analoginya...

Misalnya MinDep memiliki objek berupa mobil, maka properties dan behavior yang ada pada objek mobil adalah sebagai berikut :

Properties (bagian-bagian dari objek)
1. Pintu Mobil
2. Ban
3. Kaca Mobil
4. Wiper
5. Setir
6. Kursi Mobil, dan seterusnya.

Behavior (tingkah laku objek)
1. Bergerak Maju
2. Bergerak Mundur
3. Menambah Kecepatan
4. Mengerem
5. Membunyikan klakson, dan seterusnya.

Nah, kurang lebih begitu cara pembagiannya.

MinDep bakal sedikit bahas beberapa komponen penting yang ada di dalam OOP.

### Tentang Class

Komponen penting yang pertama yang harus kalian tau adalah class. Class adalah rancangan atau blueprint dari objek. Class digunakan hanya untuk membuat kerangka dasar dalam membuat objek.

### Instance Variable dan Reference Variable

Komponen penting selanjutnya yang harus kalian tahu dalam OOP adalah Instance Variable dan Reference Variable.

**Instance Variable** merupakan jenis variabel yang dideklarasikan di dalam class, diluar method baik constructor atau method lainnya. Instance variable dibuat ketika membuat objek. Instance variable biasanya digunakan untuk membuat properties pada objek.

**Reference Variable** merupakan jenis variabel yang merujuk pada objek. Mereferensikan objek tersebut supaya bisa digunakan pada fungsi atau method lain.

### Constructor? Apa itu?

Dan komponen terakhir yang wajib kalian ketahui adalah Constructor!

**Constructor** adalah method khusus yang akan dieksekusi pada saat pembuatan objek (instance object). Biasanya method ini digunakan untuk inisialisasi objek.

### Contohnya...

Langsung aja kita bakalan ngoding bareng lagi, kali ini MinDep bakalan ajak kalian untuk membuat sebuah objek berupa pokemon yang nantinya bisa kita mainkan dan oprek bareng-bareng.

Pertama-tama MinDep bakal membuat sebuah class sebagai kerangka dari objek yang akan kita buat. Lalu di dalam class yang sudah MinDep buat, MinDep masukkan properties pokemonnya. Untuk propertiesnya adalah nama, elemen, evolusi, exp, level, hp, dan ability.

Kemudian MinDep membuat behavior atau tingkah laku dari pokemon. Untuk behavior dari pokemon yang MinDep buat adalah ketika pokemon tersebut makan berry, diserang, dan berevolusi.

Lalu MinDep membuat constructor yang MinDep pakai untuk inisialisasi si pokemon nanti. Di dalam constructor tersebut, MinDep masukkan properties pokemon yang sudah MinDep buat sebelumnya.

Yang hasilnya bakalan seperti ini.

```dart
class Pokemon {
  // Ini Properties
  String nama;
  String elemen;
  String evolusi;
  int exp;
  int level;
  int hp;
  String ability;

  // Ini Constructor
  Pokemon(this.nama, this.elemen, this.evolusi, this.exp, this.level, this.hp,
      this.ability);

  // Ini Behavior (Tingkah Laku)
  makanBerry(int energy) {
    this.hp += energy;
    print("Si ${this.nama} mendapat tambahan energi sebesar ${energy}");
    print("Total HP : ${this.hp}");
  }

  diSerang(int kerusakan) {
    this.hp -= kerusakan;
    print(
        "Si ${this.nama} mendapat serangan nich. HP berkurang sebesar ${kerusakan}");
    print("Total HP : ${this.hp}");
  }

  berevolusi() {
    print("Si ${this.nama} evolusi gan. Jadi si ${this.evolusi}, ANJAY.");
  }
}
```

Lalu untuk menginisialisasi objek, di dalam method main MinDep buat sebuah reference variable yang digunakan untuk membuat objek baru. Kemudian di variable tersebut MinDep gunakan constructor yang sudah MinDep buat tadi, dengan nilai dari si pokemon yang mau MinDep buat.

Lalu disini MinDep sudah bisa menggunakan si pokemon tersebut, contohnya membuat pokemon melakukan tingkah lakunya atau behaviornya yang sudah MinDep buat sebelumnya.

Kurang lebih hasilnya seperti ini.

```dart
main() {
  var pikachu = Pokemon("Pikachu", "Listrik", "Raichu", 100, 8, 1000, "Nyetrum Cihuy");

  //Pikachu makan berry
  pikachu.makanBerry(8);

  //Pikachu kena serang
  pikachu.diSerang(1000);

  //Pikachu berevolusi
  pikachu.berevolusi();
}
```

*Asyik kan? coba kalian oprek-oprek sendiri dengan buat objek lainnya!*

Sampe sini dulu buat bahasan tentang Pemrograman Berorientasi Objek. MinDep gak pernah bosen mengingatkan kalo mencari itu indah, Hehehe. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*<br>
*Stay Healthy, Stay Coding. Sobat Heroes!*