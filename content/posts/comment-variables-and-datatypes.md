---
title: Commenting, Variabel, dan Tipe Data
date: "2019-11-12"
template: "post"
draft: false
slug: "/posts/comment-variables-and-datatypes/"
category: "Learn"
tags:
  - "Tipe Data"
  - "Pemrograman"
  - "Variabel"
description: "Berkenalan dengan commenting, variabel, dan tipe data"
---

Hallo Guys, MinDep is back!

Sekarang kita bakal bahas di pertemuan kedua kita beberapa minggu yang lalu. Yaitu tentang Commenting, Variabel, dan Tipe Data.

**Lesgo.**

### Commenting, Buat Apa?
Pertama kita bakalan bahas tentang Comment.

Comment atau komentar adalah catatan teks yang ditambahkan ke program untuk memberikan informasi yang jelas tentang kode yang dituliskan.

Catatan teks biasanya berupa dokumentasi yaitu untuk memberitahu kepada pemrogram (programmer) tentang kegunaan dari variabel, fungsi, dll yang sudah dibuat olehnya.

Atau penjelasan panjangnya adalah untuk mendokumentasikan program dan mengingatkan programmer tentang hal-hal rumit yang baru saja mereka lakukan dengan kode dan juga membantu programmer lainnya untuk memahami dan memelihara source code. Compiler tidak akan mengeksekusi pernyataan tersebut.

Comment dibuat dengan cara menuliskan dua garis ~~biru~~ miring "//" kemudian diikuti catatan teksnya.

```dart
void main() {
  //mengeksekusi method makan
  makan();
}

void makan() {
  //membuat sebuah variabel untuk menampung nilai lauk
  var lauk = "Tempe";
  
  //membuat sebuah variabel untuk menampung nilai sayur
  var sayur = "Sayur Asem";
  
  //menampilkan sebuah kalimat di konsol dengan variabel yang dibuat
  print('Saya makan hari ini pake lauknya $lauk dan sayurnya $sayur');
}
```


### Variabel dan Tipe Data
Lanjut lah yaa kita bahas tentang variabel dan tipe data. agak panjang nih, jadi misalnya kalo mau sambil nyemil juga oke kok hahaha.

Capcuss~

Variabel merupakan tempat menyimpan data. Sedangkan tipe data adalah jenis nilai yang akan disimpan ke dalam variabel. Variabel bersifat mutable atau berubah-ubah nilainya.

Dart memiliki beberapa tipe data special :
- Numbers = int dan double
- String
- Boolean
- Lists
- Maps
- Runes (Unicode characters dalam string)
- Symbols

Disini MinDep bakal menjelaskan beberapa jenis tipe data aja, jadi nanti untuk selebihnya bisa temen-temen cari di internet. Karena mencari itu indah.

#### 1. String
Yang pertama adalah tipe data string. Tipe data String adalah tipe data yang terdiri dari kumpulan karakter dengan panjang tertentu. Dan seringkali dianggap sebagai tipe data dasar.

Hal ini dikarenakan sampai saat ini, tipe data string paling sering digunakan oleh para programmer. Penulisan karakter string dalam bahasa pemrograman diawali dan diakhiri dengan kutip ganda (").

#### 2. Integer
Jenis tipe data satu ini dapat didefinisikan sebagai bilangan bulat. Yang artinya sebuah program yang menggunakan tipe data integer ini tidak mendukung penggunaan huruf. Naah selain itu bilangan yang digunakan juga haruslah bulat (tidak mengandung pecahan desimal). Contohnya adalah 5, -15, 100, dan lain-lain.

#### 3. Double
Jika tadi integer adalah untuk bilangan bulat, maka tipe data double ini adalah untuk bilangan desimal. Karena tipe data integer tidak mengenal bilangan pecahan atau desimal. Contoh nilai untuk tipe data double adalah 3,14 atau 7,5.

#### 4. Boolean
Tipe data boolean adalah tipe data yang hanya memiliki dua nilai yaitu TRUE (benar) dan FALSE (salah). Tipe data ini banyak digunakan untuk percabangan kode program (if else), atau untuk perintah apa yang mesti dijalankan ketika sebuah kondisi terjadi.

Cara mendeklarasikan sebuah variabel adalah dengan cara menuliskan tipe datanya terlebih dahulu, kemudian nama variabelnya dan diberi tanda sama dengan lalu yang terakhir adalah menuliskan nilainya.

Contohnya seperti ini ...

```dart
void variabel(){
  String kalimat = "Hello Dev Heroes!";
  int jumlahKakiAyam = 2;
  bool sudahMakan = true;
  double IPK = 3.5;
}
```

### Aturan Penamaan Variabel
Beberapa contoh aturan yang MinDep tau tentang penamaan variabel, yaitu :
- Nama variabel tidak dapat menggunakan keyword atau kata kunci. seperti contohnya var, if, string, integer, if, async.
- Dapat berupa campuran kata dan angka.
- Tidak dapat berisi spasi dan karakter khusus, kecuali underscore (_) dan dollar sign ($).
- Tidak boleh dimulai dengan angka.

Diatas ini adalah contoh aturan yang biasanya diterapkan dalam bahasa pemrograman Dart. Kemungkinan ada salah satu aturan diatas yang tidak berpengaruh pada bahasa pemrograman lain.

Sekali lagi MinDep ingetin, mencari itu indah hehehe.

### Final dan Const
Bonus buat temen-temen dari MinDep~

Final dan Const adalah variabel yang sifatnya tetap atau dengan kata lain, nama dan nilai dari variabel tidak bisa diubah.

Ketika kalian mendeklarasikan sebuah variabel const dan memasukkan nilainya, kemudian kalian mengganti nilai lama dengan nilai yang baru.

Contohnya adalah dibawah ini.

```dart
void main() {
  const namaku = "MinDep";
  namaku = "Mimin cakep";
}
```

Maka akan muncul error seperti ini :

**Constant variables can't be assigned a value.**

itu karena nilai dari variabel const tidak dapat diubah, sesuai dengan pernyataan diatas tadi. Dan hal ini juga berlaku pada variabel final.

Yaak itu tadi tentang commenting, variabel dan tipe data. Semogaa bermanfaat yah apa yang kita bahas dari awal tadi. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*


Sumber lain :<br>
[www.nesabamedia.com/pengertian-tipe-data/](https://www.nesabamedia.com/pengertian-tipe-data/)