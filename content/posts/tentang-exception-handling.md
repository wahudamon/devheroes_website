---
title: Menangani Runtime Error dengan Exception Handling
date: "2019-12-02"
template: "post"
draft: false
slug: "/posts/tentang-exception-handling/"
category: "Learn"
tags:
  - "DartLang"
  - "Pemrograman"
  - "Exception"
description: "Biar aplikasi buatanmu ga gampang force close."
---

Holaaa, MinDep is back!

Gimana? Kemaren seru nggak pertemuannya?
Okee, kali ini mindep bakalan review materi kemarin yaitu tentang Exception Handling dan Object Oriented Programming.

Tapi dikarenakan materinya lumayan padat, jadi MinDep bagi jadi dua artikel yah! Jadi untuk bagian ini, kita bahas dulu tentang Exception Handling.

Langsung aja gausah lama-lama.

Sebelum kita bahas tentang Exception Handling, ada baiknya kita bahas dulu tentang Exception.

### Apa itu Exception?

Jadi begini, Exception itu adalah sebuah error atau kesalahan yang tidak diinginkan yang terjadi pada saat runtime (program dijalankan). Yang kemudian mengganggu aliran kode yang sedang dieksekusi.

Hal ini dapat menyebabkan aplikasi atau program menjadi berhenti tiba-tiba. Atau yang lebih parah lagi adalah menjadi berhenti secara tiba-tiba (stopped working).

Disebabkan karena kesalahan dari programmer atau kesalahan input (masukkan) dari pengguna. Contohnya adalah ketika kita membuat aplikasi kalkulator yang digunakan pada platform PC. Dan kita lupa memberikan peringatan bahwa tidak boleh memasukkan karakter lain selain angka.

Maka ketika pengguna menggunakan aplikasi kita, kemungkinan besar mereka akan memasukkan karakter selain angka seperti huruf. Nah ketika masukkan dari pengguna diproses pada aplikasi dan ternyata tidak sesuai dengan format yang dibuat, maka akan terjadi sebuah exception yang bisa menyebabkan program menjadi berhenti secara tiba-tiba.

### Exception ada jenisnya?

Ada beberapa jenis Exception yang dapat dideteksi oleh bahasa pemrograman dart yaitu :
1. **IntegerDivisionByZeroException** - Terjadi ketika ada angka yang dibagi dengan nol.

2. **FormatException** - Terjadi ketika sebuah string atau beberapa data lain tidak memiliki sebuah format yang sudah ditentukan sehingga tidak dapat diproses.

3. **IOException** - Terjadi ketika ada kesalahan input-output.

4. **DeferredLoadException** - Terjadi ketika sebuah deffered library gagal dimuat.

5. **Timeout** - Terjadi ketika sebuah timeout yang sudah dijadwalkan terjadi ketika menunggu sebuah hasil data yang diambil secara asynchronous.

### Tentang Exception Handling

Jadi metode untuk menangani Exception yang terjadi dinamakan **Exception Handling**. Jadi dengan metode ini, kita bisa mencegah aplikasi berhenti berjalan (stopped working) ketika terjadi Exception dan menampilkan Exception tersebut kepada pengguna dengan bahasa yang mudah dimengerti oleh mereka.

### Ada berapa jenis metode Exception Handling?

Ada dua jenis metode Exception Handling yang MinDep tau, yaitu :
1. **Try-Catch**

Try-Catch adalah salah satu metode Exception Handling yang mekanismenya adalah seperti ini. Si Programmer awalnya memasukkan baris kode yang dirasa akan menimbulkan Exception ke dalam blok try. Kemudian membuat blok catch dengan parameter yang digunakan untuk menampung nilai berupa exception atau error yang terjadi.

Baris kode akan dieksekusi dan diperiksa di dalam baris try. Kemudian ketika terjadi Exception atau Error, maka secara otomatis Error atau Exception itu dikirimkan ke parameter yang ada di dalam blok catch.

Lalu di dalam blok catch, programmer bisa menambahkan perintah untuk menampilkan error yang terjadi kepada pengguna. Tentunya dengan bahasa yang dapat dimengerti pengguna.

2. **Custom Exception Handling Class**

jadi sebenarnya di dalam bahasa pemrograman dart itu sudah terdapat built-in method untuk menangani beberapa Exception yang terjadi. Tapi tidak semua Exception bisa ditangani, jadi ada lagi metode Exception Handling yang bernama Custom Exception Handling Class.

Pada metode ini, kita bisa membuat Exception Handling sendiri. Sesuai dengan logika dan projek yang sedang dikerjakan. Cara membuatnya adalah dengan membuat sebuah class yang ditambahkan implement ke class exception agar class yang kita buat bisa mewarisi sifat-sifat dari class exception.

Ini juga merupakan salah satu konsep yang ada di pemrograman berorientasi objek yaitu konsep inheritance atau pewarisan sifat. Kemudian di dalam class tersebut kita buat sebuah fungsi yang menampung pesan yang akan keluar ketika exception terjadi dan class yang kita buat tadi dipanggil.

Contohnya begini.

```dart
class NilaiNegatifException implements Exception {
  String pesanError() {
    return 'Nilai yang anda masukkan tidak boleh negatif!';
  }
}
```

### Contohnya...

Kali ini MinDep akan memberi contoh dengan menggunakan metode Try-Catch, dan untuk Exceptionnya adalah yang **IntegerDivisionByZeroException**.

Pertama MinDep akan buat sebuah dart console project baru. Lalu MinDep akan buka file main.dart yang ada di folder bin. MinDep edit dan tambahkan fungsi baru yang bernama integerDividedbyZero dengan sebuah parameter bertipe data integer yang akan menerima masukkan dari pengguna berupa angka.

Kemudian MinDep buat blok Try-Catch dan MinDep masukkan baris kodenya ke dalam blok try. Dan di dalam blok catch, MinDep buat custom alert yang menampilkan pesan error bahwa angka tidak dapat dibagi dengan nol.

Lalu fungsi tersebut MinDep panggil di dalam method main. Dan jadinya kira-kira begini.

```dart
main() {
  integerDividedbyZero(-2);
}

int integerDividedbyZero(int angka) {
  try {
    int hasil = angka ~/ 0;
    return hasil;
  } catch (e) {
    print("Tidak bisa membagi angka dengan 0!");
  }
}
```

*Hasilnya gimana min? Coba sendiri ya :p*

Sampe sini dulu buat bahasan tentang Exception Handling. MinDep gak pernah bosen mengingatkan kalo mencari itu indah, Hehehe. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*<br>
*Stay Healthy, Stay Coding. Sobat Heroes!*