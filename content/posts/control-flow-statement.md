---
title: Control Flow Statement
date: "2019-11-13"
template: "post"
draft: false
slug: "/posts/control-flow-statement/"
category: "Learn"
tags:
  - "Percabangan"
  - "Pemrograman"
  - "Aliran Data"
description: "Mengatur jalannya program dengan Control Flow Statement"
---

**Yoo, MinDep is back!**

Apa kabar kalian semuanya? Masih semangat ya belajarnya?

Okay setelah kemarin kita bahasa tentang variabel dan tipe data, yuk kita naik satu tingkat lebih jauh lagi. Sekarang kita bakal belajar tentang Control Flow Statement.

Nah, apaan tuh Control Flow Statement? Kok kayaknya ribet banget istilahnya?

Jadi Control Flow Statement adalah sebuah metodde yang digunakan untuk mengatur jalannya program pada kondisi tertentu.

Analoginya itu kayak gini, misalnya temen-temen mau pergi ke rumah sakit. Kemudian ditengah jalan kalian ketemu sebuah persimpangan. Nah disini Konsep Control Flow Statement bekerja.

Karena memang kondisinya kalian sedang menuju ke rumah sakit, maka kalian akan memilih arah yang menuju ke rumah sakit di persimpangan itu.

Jadi dengan Control Flow Statement, program akan mengeksekusi perintah berdasarkan kondisi yang sudah ditentukan.

Keyword yang digunakan untuk metode Control Flow Statement adalah :
- **IF ELSE**

IF ELSE biasanya digunakan ketika hanya terdapat dua kondisi. Contohnya TRUE dan FALSE atau Bahasa Indonesianya itu Benar dan Salah.

MinDep langsung kasih contohnya yaa :

```dart
main() {
  //Jumlah uang MinDep
  int uang = 50000;

  //Control Flow Statement dengan IF ELSE
  if(uang > 50000) {
    //Jika uang MinDep diatas 50000 maka tampilkan kalimat ini
    print('Saatnya makan di Kaefsi!');
  } else {
    //Jika uang MinDep dibawah 50000 maka tampilkan kalimat ini
    print('Ke burjo dulu aja deh hari ini.');
  }
}
```

Jadi begini penjelasannya. Pertama-tama MinDep punya uang 50000, nah kondisi yang dibuat adalah ketika uang MinDep **diatas** 50000 maka MinDep akan makan di Kaefsi. Jika uang MinDep **dibawah** 50000, maka MinDep akan memilih makan di burjo dulu hari ini.

Nah kondisi yang sudah dibuat tadi kemudian dituliskan ke dalam bentuk IF ELSE. Kondisi pertama yaitu **ketika nilai dari variabel uang lebih dari 50000** MinDep masukkan kedalam tanda kurung di sebelah IF.

Jadi ketika nilai uang sebenarnya sesuai dengan kondisi yang ada di dalam IF, kode yang ada di dalam blok IF akan dieksekusi program. ELSE disini maksudnya adalah kondisi apapun selain yang ada didalam IF.

Nah maka ketika kondisinya uang MinDep kurang dari 50000 yang mana itu merupakan kondisi selain yang ada di dalam IF, maka akan mengeksekusi kode yang ada di dalam blok ELSE.

- **SWITCH CASE**

Switch Case merupakan jenis Control Flow Statement yang dirancangan khusus untuk menangani pengambilan keputusan yang melibatkan sejumlah atau banyak alternatif penyelesaian. Penggunaannya untuk memeriksa data yang bertipe karakter atau integer (numerik).

*Switch Case biasanya digunakan untuk percabangan yang kondisinya banyak. Karena Switch Case bisa menangani kondisi yang banyak atau lebih dari dua.*

*It's analogi time!*

Jadi kondisi disini ibaratnya seperti nilai temen-temen di kampus. Jadi kalau misalnya mendapatkan **nilai A** itu predikatnya **Sangat Baik**, **nilai B** itu **Baik**, **nilai C** itu **Cukup**, dan **nilai D** itu **Kurang Baik**.

*Langsung kita buat programnya!*

```dart
main() {
  var nilai = "A";

  switch(nilai) {
    case 'A':
      print("Sangat Baik");
      break;
    case 'B':
      print("Baik");
      break;
    case 'C':
      print("Cukup");
      break;
    case 'D':
      print("Kurang Baik");
      break;
    default:
      print("Nilai yang anda masukkan tidak sesuai!");
      break;
  }
}
```

MinDep jelasin yaa. Jadi ketika kalian memasukkan nilai pada variabel nilai, nah kemudian nilai tersebut akan masuk kedalam switch yang nantinya akan diperiksa apakah sama dengan salah satu case yang sudah dibuat.

Jika nilainya sama, maka akan menjalankan perintah yang ada di dalam case itu. Jika tidak ada yang sama, maka akan menjalankan perintah yang ada di case default.

Jadi karena nilai yang kita masukkan adalah A, maka SWITCH CASE akan memeriksa case yang ada apakah sama dengan nilai yang kita milikki. Dan akhirnya ketemu, yaitu di case pertama. Karena sama-sama berisi nilai 'A', maka perintah print("Sangat Baik"); pun akan dijalankan. Yang akhirnya menampilkan kalimat Sangat Baik pada konsol.

Yaak itu tadi sedikit tentang control flow statement. Semogaa bermanfaat yah apa yang kita bahas dari awal tadi. MinDep ingetin juga kalo DevHeroes bakal ngadain kegiatan belajar bareng setiap Jumat Malam.

Buat kalian yang mau tanyain tentang penjelasan MinDep diatas, bisa dateng ke pertemuan kita atau tanya langsung ke akun official telegram DevHeroes.

*Thanks for stopping by, MinDep pamit. Cabs!*