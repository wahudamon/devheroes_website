---
title: "Kegiatan Kita Saat Ini"
template: "page"
---

###Eh, Halo lagi Netizen!

Developer Heroes punya kegiatan yang kalian bisa ikuti nih, simak ya!

**BarBar (Belajar Bareng) DevHeroes**<br>
Didalam kegiatan ini kita bakal berbagi ilmu kepada temen-temen. Konsepnya hampir sama kayak belajar dikelas. Cuman bedanya disini gaada tekanan, indikator kelulusan,.... dan nilai. *We're just learning and having fun together bruh, hahaha*.
<br><br>Dan mungkin kalau kalian juga pengen sharing ke temen-temen yang lain juga bisa dilakuin disini. Temen-temen yang ikutan bisa tanya dan buat diskusi untuk bertukar pikiran tentang materi yang lagi dipelajari atau bahasan yang lainnya yang lingkupnya masih di bidang pemrograman dan teknologi.

*Intinya gaada yang lebih pinter disini dan let's learn together!*

Beberapa topik yang bakalan kita bahas disini :
- Dasar Pemrograman
- Konsep Pemrograman Berorientasi Objek
- Bahasa Pemrograman Dart