---
title: "Tentang DevHeroes"
template: "page"
---

## Halo Netizen!
Perkenalkan, kami adalah Developer Heroes atau disingkat DevHeroes!

DevHeroes adalah sebuah komunitas yang berperan sebagai wadah untuk para mahasiswa khususnya di Jurusan Teknik Informatika Universitas Muhammadiyah Yogyakarta dalam mempelajari ilmu pemrograman.

Penasaran tentang Kegiatan DevHeroes itu apa aja? langsung klik kata **Activities** aja disamping, atau atau kepoin aja akun instagram kita. Caranya tinggal klik icon instagram dikolom profil kita!

Atau mau kepo lebih dalam tentang DevHeroes dan kegiatan kita? Bisa langsung kirim pesan ke mimin ganteng kita via telegram, tinggal klik icon telegram yang ada di kolom profil kita aja!