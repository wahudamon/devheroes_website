const PAGINATION = {
  PREV_PAGE: '← PREVIOUS',
  NEXT_PAGE: '→ NEXT'
};

export default PAGINATION;
